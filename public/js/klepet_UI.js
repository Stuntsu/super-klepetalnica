/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
 
   var mapTableVzdevki = [];
  var mapTableNadimki = [];
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}
function dodajSlike(sporocilo){
  var besede = sporocilo.split(' ');
  var tabela = besede;
  var stSlik = 0;
  for(var i in besede){
    var tb = besede[i]; // tb == trenutnaBeseda
    tabela[i] = 0;
    if(tb.substring(0, 7).toLowerCase() == 'http://' || tb.substring(0, 8).toLowerCase() == 'https://'){
      //console.log("Found pic");
      
      if(tb.substring(tb.length - 4, tb.length) == '.jpg' ||
         tb.substring(tb.length - 4, tb.length) == '.png' ||
         tb.substring(tb.length - 4, tb.length) == '.gif' ){
        if(stSlik == 0){
          sporocilo += ' ';
        }
        sporocilo += '<img width="200px" style="margin-left: 20px" src="' + tb + '" />';
        //tabela[i] = tb;
        stSlik++;
      }else if(
         tb.substring(tb.length - 5, tb.length - 1) == '.jpg' || // se konca z locilom
         tb.substring(tb.length - 5, tb.length - 1) == '.png' ||
         tb.substring(tb.length - 5, tb.length - 1) == '.gif'){
        if(stSlik == 0){
          sporocilo += ' ';
        }
        sporocilo += ' <img width="200px" style="margin-left: 20px" src="' + tb.substring(0, tb.length - 1) + '" />';
        //tabela[i] = tb.substring(0, tb.length - 1);
        stSlik++;
      }
    }
    
  }
  return sporocilo;
}

function zaSliko(sporocilo){
  var besede = sporocilo.split(' ');
  for(var i in besede){
    var tb = besede[i];
    if(tb.substring(0, 7).toLowerCase() == 'http://' || tb.substring(0, 8).toLowerCase() == 'https://' 
    || tb.substring(1, 8).toLowerCase() == 'http://' || tb.substring(1, 9).toLowerCase() == 'https://'){
      if(tb.substring(tb.length - 4, tb.length) == '.jpg' ||
           tb.substring(tb.length - 4, tb.length) == '.png' ||
           tb.substring(tb.length - 4, tb.length) == '.gif' ){
          
          return true;
        }else if(
           tb.substring(tb.length - 5, tb.length - 1) == '.jpg' || // se konca z locilom
           tb.substring(tb.length - 5, tb.length - 1) == '.png' ||
           tb.substring(tb.length - 5, tb.length - 1) == '.gif'){
          
          
          return true;
        }
      }
  }
  
  return false;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = zaSliko(sporocilo);
  //console.log(zaSliko(sporocilo));
  if (jeSmesko) {
    
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    
    return divElementHtmlTekst(sporocilo);
  }
  if(jeSlika){
    sporocilo = sporocilo
    .split("<").join("&lt;")
    .split(">").join("&gt;")
    .split("&lt;img").join("<img")
    .split('png" /&gt;').join('png"><br />')
    .split('jpg" /&gt;').join('jpg"><br />')
    .split('gif" /&gt;').join('gif"><br />');
    
    //console.log(sporocilo);
  
    
    return divElementHtmlTekst(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSlike(sporocilo);
  
  
  
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    
    
    if(sporocilo.substring(0, 8).toLowerCase() == '/zasebno'){
      var besede = sporocilo.split(' ');
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      // parametri[1] je nickname.
      var nick = parametri[1];
      
      for(var i in mapTableVzdevki){
        if(mapTableVzdevki[i] == nick){
          nick = mapTableNadimki[i] + "(" + mapTableVzdevki[i] + ")";
        }
      }
      
      var msg = '/zasebno "' + nick + '" "' + parametri[3] + '"';
      sporocilo = msg;
      
    }
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      sistemskoSporocilo = dodajSlike(sistemskoSporocilo);
      //$('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#sporocila').append(divElementEnostavniTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}
function dodajNadimek(sporocilo){
  
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {

    if(sporocilo.HTML){
      var nick = sporocilo.HTML;
      for(var i in mapTableVzdevki){
        if(mapTableVzdevki[i] == nick){
          nick = mapTableNadimki[i] + "(" + mapTableVzdevki[i] + ")";
        }
      }
      var novEl = "<div>"+ nick + " (zasebno): &#9756;</div>";
      $('#sporocila').append(novEl);
    }else if(sporocilo.samseb){
      var nick = sporocilo.samseb;
      for(var i in mapTableVzdevki){
        if(mapTableVzdevki[i] == nick){
          nick = mapTableNadimki[i] + "(" + mapTableVzdevki[i] + ")";
        }
      }
      var novEl = "<div>(zasebno za " + nick + "): &#9756;</div>";
      $('#sporocila').append(novEl);
    }else{
      
      var besede = sporocilo.besedilo.split(':');
      console.log(sporocilo.besedilo);
      var skupaj = "";
      var preslikava = 0;
      
      for(var i in mapTableVzdevki){
        if(mapTableVzdevki[i] == besede[0]){
          besede[0] = mapTableNadimki[i] + "(" + mapTableVzdevki[i] + ")";
          preslikava = 1;
          
        }else if(mapTableVzdevki[i] == besede[0].substring(0, besede[0].length - 10)){
          besede[0] = mapTableNadimki[i] + "(" + mapTableVzdevki[i] + ") (zasebno)";
          preslikava = 1;
        }
      }
      skupaj = besede.join(':');
      
      var novElement = divElementEnostavniTekst(skupaj);
      $('#sporocila').append(novElement);
    }
    
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  //////////////////////////////////////////////////////////////////////////////////////////
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var x = 0;
      for(var j in mapTableVzdevki){
        if(uporabniki[i] == mapTableVzdevki[j]){
          //console.log(mapTableVzdevki[j]);
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(mapTableNadimki[j] + "(" + mapTableVzdevki[j] + ")"));
          //console.log("preimenoval")
          x = 1;
        }
        //console.log(mapTableVzdevki[j]);
      }
      //console.log(mapTableVzdevki);
      //console.log(mapTableNadimki);
      if(x == 0){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }

    }
    $('#seznam-uporabnikov div').click(function() {
      var ciljniVzdevek = $(this).text();
      
      var vsi = ciljniVzdevek.split("(");
      var vzdevk = (vsi[vsi.length - 1].split(")"))[0];
      
      socket.emit('sporocilo', {
        krcanec: vzdevk,
        posiljatelj: trenutniVzdevek
        
      } );
     // console.log("klice se socket.emit (mnde)");
      
    });
  });

  socket.on('preimenuj', function(data){
    //console.log("prisel socket preimenuj z vzdevkom: " + data.vzdevek + " in preimenujemo v " + data.nadimek);
    var vzdevek = data.nick;
    var nadimek = data.nadimek;
    
    var jeRes = 0;
    for(var i in mapTableVzdevki){
      if(mapTableVzdevki[i] == vzdevek){
        mapTableNadimki[i] = nadimek;
        jeRes = 1;
      }
    }
    if(jeRes == 0){
      mapTableVzdevki.push(vzdevek);
      mapTableNadimki.push(nadimek);
    }
    //console.log("mapTableVzdevki:");
    //console.log(mapTableVzdevki);
    //console.log("mapTableNadimki:")
    //console.log(mapTableNadimki);
    
  })
  //////////////////////////////////////////////////////////////////////////////////////////
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
});
